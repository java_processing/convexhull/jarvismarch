enum EnumSide {
  Left, Right;
  
  public static float getSin(PVector beginEdge, PVector endEdge, PVector point) {
    PVector vectHull = PVector.sub(beginEdge, endEdge).normalize();
    PVector vectPrcsdPoint = PVector.sub(beginEdge, point);
    return (vectHull.cross(vectPrcsdPoint)).z / PVector.dist(beginEdge, point);
  }

  public static EnumSide whichSide(PVector beginEdge, PVector endEdge, PVector point) {
    if (getSin(beginEdge, endEdge, point) < 0) {
      return EnumSide.Left;
    } else {
      return EnumSide.Right;
    }
  }
}
