class ExtrapolateEdgeByStep extends Step {

  protected PVector cvxHullPnt;
  protected PVector cvxHullPnt2;  
  protected PVector currentMaxLeftPnt = new PVector();
  protected float currentMaxLftSin = 0;
  protected int indxCorner;

  ExtrapolateEdgeByStep(ConvexHull cvxHull, int indxCrn) {
    super(cvxHull);
    this.indxCorner = indxCrn;
    this.cvxHullPnt = convexHull.getPoint(indxCorner);
    this.cvxHullPnt2 = convexHull.getPoint(indxCorner+1);
  }

  public boolean update() {

    if (pntsToProcessIterator.hasNext()) {
      
      beingPrcsdPoint = pntsToProcessIterator.next();
      
      float sinValue = EnumSide.getSin(cvxHullPnt, cvxHullPnt2, beingPrcsdPoint);
      
      if (sinValue < currentMaxLftSin) {
        int indxToReplace = convexHull.indexOf(currentMaxLeftPnt);
        
        if (indxToReplace == -1) {
          convexHull.add(indxCorner+1, beingPrcsdPoint);
        } else {
          convexHull.set(indxToReplace, beingPrcsdPoint);
        }
        currentMaxLftSin = sinValue;
        currentMaxLeftPnt = beingPrcsdPoint;
      }
      return false;
    } else {
      indxCorner++;
      if (convexHull.getPoint(indxCorner) != convexHull.get(0)) {
        convexHull.setStep(new ExtrapolateEdgeByStep(convexHull, indxCorner));
        return false;
      } else {
        return true;
      }
    }
  }

  public void show() {
    super.show();
    stroke(0, 126, 255);
    strokeWeight(5);
    try {
      connect(cvxHullPnt, cvxHullPnt2);
      connect(cvxHullPnt, beingPrcsdPoint);
    } 
    catch (NullPointerException e) {
    }
  }
}
